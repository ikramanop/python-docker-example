FROM python:alpine

RUN apk add --update --no-cache make

WORKDIR /app

COPY . /app

RUN make install-dependencies

EXPOSE 5000

CMD [ "python3", "main.py" ]

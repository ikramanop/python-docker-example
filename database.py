import pymysql
import os


def get_connection():
    mysql_host = os.getenv("MYSQL_HOST")
    if mysql_host is None:
        mysql_host = '127.0.0.1'

    mysql_port = os.getenv("MYSQL_PORT")
    if mysql_port is None:
        mysql_port = '3306'

    mysql_user = os.getenv("MYSQL_USER")
    if mysql_user is None:
        mysql_user = 'root'

    mysql_password = os.getenv("MYSQL_PASSWORD")
    if mysql_password is None:
        mysql_password = 'root'

    mysql_database = os.getenv("MYSQL_DATABASE")
    if mysql_database is None:
        mysql_database = 'default'

    connection = pymysql.connect(
        host=mysql_host,
        port=int(mysql_port),
        user=mysql_user,
        password=mysql_password,
        database=mysql_database
    )

    with connection.cursor() as cursor:
        cursor.execute(
            """
            create table if not exists cars (
                id int primary key auto_increment,
                mark varchar(15) not null,
                model varchar(15),
                number varchar(10) not null,
                unique(number)
            )
            """
        )

    return connection

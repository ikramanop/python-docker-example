from database import get_connection
from flask import Flask, request
import threading

app = Flask(__name__)

connection = get_connection()


def run_server(host, port):
    server = threading.Thread(target=app.run, kwargs={
                              'host': host, 'port': port})
    server.start()
    return server


def shutdown_server():
    terminate = request.environ.get('werkzeug.server.shutdown')
    if terminate:
        terminate()


@app.route('/get/car/<id>', methods=['GET'])
def get_car_by_id(id):
    with connection.cursor() as cursor:
        query = "select * from `cars` where `id`=%s"
        cursor.execute(query, (int(id),))

        data = cursor.fetchone()

        if data is None:
            return {
                "type": "error",
                "message": "car not found"
            }, 400

        return {
            "mark": data[1],
            "model": data[2],
            "number": data[3]
        }, 200


@app.route('/add/car', methods=['POST'])
def add_car():
    error = {
        "type": "error",
        "message": "validation error"
    }

    if len(request.json.keys()) < 2 and len(request.json.keys()) > 3:
        return error, 400

    if 'mark' not in request.json.keys():
        return error, 400

    if 'number' not in request.json.keys():
        return error, 400

    with connection.cursor() as cursor:
        if 'model' not in request.json.keys():
            query = "insert into `cars` (`mark`, `number`) values (%s, %s)"
            print(tuple(request.json.values()))
            cursor.execute(query, tuple(request.json.values()))
        else:
            query = "insert into `cars` (`mark`, `model`, `number`) values (%s, %s, %s)"
            cursor.execute(query, tuple(request.json.values()))

        return {
            "success": True
        }, 200


@app.route('/shutdown', methods=['POST'])
def shutdown():
    shutdown_server()
    return 'Shutting down...'


if __name__ == '__main__':
    run_server(host='0.0.0.0', port=5000)

export APP_NAME=test-flask-server

run:
	python3 main.py

install-dependencies:
	pip3 install -r requirements.txt

build-docker:
	docker build -t ${APP_NAME} .

run-docker:
	docker run -d -p 5000:5000 ${APP_NAME}

run-compose:
	docker-compose up -d

down-compose:
	docker-compose down